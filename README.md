Installation guide
===================

1. **Update packages**

    composer update

2. **Set records to parameters.yml config file**

    `admin_name: Admin`

    `admin_email: admin@admin.hu`

3. **Create db if not exists**

    `app/console doctrine:database:create`

4. **Install assets**

    `app/console assets:install web --symlink`

    `app/console assetic:dump --force`

5.  **Create db tables**

    `app/console doctrine:schema:update --force`

6. Insert test records

    `app/console forum:set:banned `

    `app/console forum:set:topics`

7. remove directory /app/cache/prod/






