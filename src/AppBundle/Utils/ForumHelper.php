<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 17.05.2016
 * Time: 17:17
 */

namespace AppBundle\Utils;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class ForumHelper
{
    protected $em;
    protected $adminName;
    protected $adminEmail;
    protected $session;
    
    public function __construct(EntityManager $em, Session $session, $adminName, $adminEmail)
    {
        $this->em = $em;
        $this->adminEmail = $adminEmail;
        $this->adminName = $adminName;
        $this->session = $session;
    }
    
    public function hasRightName($name, $email){
        if($name===$this->adminName
            && $email!==$this->adminEmail){
            $this->session->getFlashBag()->add('danger', 'You can`t use this name!');
            return false;
        }
        return true;
    }

    public function hasBannedWords($text){

        $bannedWords = $this->em->getRepository('AppBundle:BannedWords')->findAll();
        foreach ($bannedWords as $banned){
            if(strpos($text, $banned->getName())!==false){
                $this->session->getFlashBag()->add('danger', 'You have banned words in your comment!');
                return true;
            }
        }
        return false;
    }
}