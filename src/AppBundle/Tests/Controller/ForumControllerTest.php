<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ForumControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testTopic()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/topic');
    }

    public function testTasks()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tasks');
    }

    public function testComments()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/comments');
    }

}
