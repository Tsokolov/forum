<?php

namespace AppBundle\Command;

use AppBundle\Entity\Topic;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForumSetTopicsCommand extends ContainerAwareCommand
{
    protected $topics = [
        'PHP',
        'MySQL',
        'HTML',
        'CSS',
        'JavaScript'
    ];
    protected function configure()
    {
        $this
            ->setName('forum:set:topics')
            ->setDescription('Set topics to the database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach ($this->topics as $topic){
            $t = new Topic();
            $t->setName($topic);
            $em->persist($t);
            $output->writeln('Set: '.$topic);
        }
        $em->flush();
        $output->writeln('Done!');
    }

}
