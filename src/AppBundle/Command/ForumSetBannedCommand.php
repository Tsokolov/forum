<?php

namespace AppBundle\Command;

use AppBundle\Entity\BannedWords;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForumSetBannedCommand extends ContainerAwareCommand
{
    protected $banned = [
        'censored0',
        'censored1',
        'censored2',
        'censored3'
    ];
    protected function configure()
    {
        $this
            ->setName('forum:set:banned')
            ->setDescription('Set test banned words to the database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach ($this->banned as $banned){
            $bWord = new BannedWords();
            $bWord->setName($banned);
            $em->persist($bWord);
            $output->writeln('Set: '.$banned);
        }
        $em->flush();
        $output->writeln('Done!');
    }

}
