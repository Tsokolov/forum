<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Task;
use AppBundle\Entity\Topic;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class ForumController extends Controller
{
    /**
     * @Route("/", name="forum_topics")
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function indexAction()
    {
        $this->_topicsTable();
        return $this->render('AppBundle:Forum:index.html.twig', array(// ...
        ));
    }


    /**
     * @Route("/topic/{id}/tasks", name="forum_topic")
     */
    public function tasksAction($id, Request $request)
    {
        $comment = new Comment();
        $form = $this->createCommentForm($comment);
        $form->handleRequest($request);
        /** @var EntityManager $em */
        $em = $this->get('doctrine')->getManager();

        /** @var Topic $topic */
        $topic = $em->getRepository('AppBundle:Topic')->find($id);
        if ($form->isValid() && $form->isSubmitted()) {
            $helper = $this->get('app_bundle.forum_helper');
            /** @var Comment $comment */
            $comment = $form->getData();
            $comment->setTopic($topic);

            if (!$helper->hasBannedWords($comment->getComment())
                && $helper->hasRightName($comment->getName(), $comment->getEmail())
            ) {
                /** @var Task $task */
                $task = new Task();
                $task->setInitComment($comment);
                $task->setLastComment($comment);
                $task->setCreatedAt(new \DateTime());
                $task->setTopic($topic);
                $task->setStatus(Task::STATUS_OPEN);
                $comment->setTask($task);

                $em->persist($comment);
                $em->flush();
                $form = $this->createCommentForm(new Comment());
            }
        }
        $this->_tasksTable($id);
        return $this->render('AppBundle:Forum:tasks.html.twig', array(
            'form' => $form->createView(),
            'topic' => $topic
        ));
    }

    private function createCommentForm($comment)
    {
        return $this->createFormBuilder($comment)
            ->add('name', TextType::class)
            ->add('email', EmailType::class, [
                'required' => true
            ])
            ->add('comment', TextareaType::class)
            ->add('create', SubmitType::class, [
                'label' => 'Create',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();
    }

    /**
     * @Route("/task/{id}/comments", name="forum_comments")
     */
    public function commentsAction($id, Request $request)
    {
        $comment = new Comment();
        $form = $this->createCommentForm($comment);
        $form->handleRequest($request);
        $em = $this->get('doctrine')->getManager();
        /** @var Task $task */
        $task = $em->getRepository('AppBundle:Task')->find($id);
        if ($form->isValid() && $form->isSubmitted()) {
            /** @var Comment $comment */
            $comment = $form->getData();
            $helper = $this->get('app_bundle.forum_helper');
            if (!$helper->hasBannedWords($comment->getComment())
                && $helper->hasRightName($comment->getName(), $comment->getEmail())
            ) {
                /** @var Comment $lastComment */
                $lastComment = $task->getLastComment();
                //dump($task->getLastComment());
                if (
                    $comment->getName() === $lastComment->getName()
                    && $comment->getEmail() === $lastComment->getEmail()
                    && $comment->getComment() !== $lastComment->getComment()
                ) {
                    $lastComment->setComment($lastComment->getComment() . '<hr/>' . $comment->getComment());
                    $comment = $lastComment;
                } else {
                    $comment->setTopic($task->getTopic());
                    $task->setLastComment($comment);
                    $comment->setTask($task);
                }
                $em->persist($comment);
                $em->flush();
                $form = $this->createCommentForm(new Comment());
            }
        }
        $this->_commentsTable($task->getId());
        return $this->render('AppBundle:Forum:comments.html.twig', array(
            'form' => $form->createView(),
            'task' => $task
        ));
    }

    /**
     * @Route("/topics/grid", name="forum_topics_grid")
     */
    public function topicsGridAction()
    {
        return $this->_topicsTable()->execute();
    }

    /**
     * @Route("/topic/{id}/grid", name="forum_tasks_grid")
     */
    public function tasksGridAction($id)
    {
        return $this->_tasksTable($id)->execute();
    }

    /**
     * @Route("/task/{id}/grid", name="forum_comments_grid")
     */
    public function commentsGridAction($id)
    {
        return $this->_commentsTable($id)->execute();
    }

    /**
     *
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    private function _topicsTable()
    {
        return $this->get('datatable')
            ->setEntity("AppBundle:Topic", "x")
            ->setFields(
                array(
                    "Name" => 'x.name',
                    "Comments" => 'x.comments',
                    "_identifier_" => 'x.id')
            )
            ->setRenderers([
                1 => [
                    'view' => 'AppBundle:Renderers:topic_comments_row.html.twig'
                ]
            ])
            ->setHasAction(false);
    }

    /**
     *
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    private function _tasksTable($topic_id)
    {
        return $this->get('datatable')
            ->setEntity("AppBundle:Task", "x")
            ->setFields(
                [
                    "Name" => 'x.initComment',
                    "Date" => 'x.createdAt',
                    //'ID'=>'x.id',
                    "Comment" => 'x.initComment',
                    "Comments" => "x.comments",
                    'Solved' => "x.status",
                    "_identifier_" => 'x.id']
            )
            ->setWhere(
                'x.topic = :topic_id',
                ['topic_id' => $topic_id]
            )
            ->setRenderers([
                0 => [
                    'view' => 'AppBundle:Renderers:task_name_row.html.twig'
                ],
                1 => [
                    'view' => 'AppBundle:Renderers:task_date_row.html.twig'
                ],
                2 => [
                    'view' => 'AppBundle:Renderers:task_comment_row.html.twig'
                ],
                3 => [
                    'view' => 'AppBundle:Renderers:task_comments_row.html.twig'
                ],
                4 => [
                    'view' => 'AppBundle:Renderers:task_status_row.html.twig'
                ]
            ])
            ->setHasAction(false);
    }

    /**
     *
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    private function _commentsTable($task_id)
    {
        return $this->get('datatable')
            ->setEntity("AppBundle:Comment", "x")
            ->setFields(
                array(
                    "Name" => 'x.name',
                    "Comment" => 'x.comment',
                    "Email" => 'x.email',
                    "ID" => 'x.id',
                    "_identifier_" => 'x.id')
            )
            ->setWhere(
                'x.task=:task_id',
                ['task_id' => $task_id]
            )
            ->setOrder('x.id', 'asc')
            ->setHasAction(false);
    }

}
